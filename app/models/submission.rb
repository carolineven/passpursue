class Submission < ActiveRecord::Base
  has_many :reviews

  validates :user_id, presence: true
  validate :has_submission

  def has_submission
    if !self.link.present? && !self.file_data.present?
      self.errors[:base] << 'Need at least a submission link or file.'
    end
  end

  def getCandidate
    if user_id.nil?
      @candidate = nil
    else
      @candidate = User.find(user_id)
    end
  end

  def getCandidateEmail
    if user_id.nil?
      @candidate_email = 'unknown'
    else
      @candidate = User.find(user_id).email
    end
  end
end
