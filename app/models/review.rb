class Review < ActiveRecord::Base
  belongs_to :submission

  VERDICT = %w(Pass Pursue)

  validates :user_id, presence: true
  validates_inclusion_of :verdict, :in => VERDICT


  def getReviewer
    if user_id != nil
      @reviewer = User.find(user_id)
    end
  end

  def getReviewerEmail
    if user_id != nil
      @reviewer_email = User.find(user_id).email
    else
      @reviewer_email = 'unknown'
    end
  end
end
