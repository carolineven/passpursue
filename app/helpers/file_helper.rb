require 'fileutils'

module FileHelper

  def create_dir_struc(path)
    dirname = File.dirname(path)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end
  end

end