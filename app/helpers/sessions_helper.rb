module SessionsHelper

  # Logs in the given user.
  def log_in(signup)
    session[:signup_id] = signup.id
  end

  # Returns the current logged-in user (if any).
  def current_user
    @current_user ||= Signup.find_by(id: session[:signup_id])
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end

  def log_out
    session.delete(:signup_id)
    @current_user = nil
  end

  def authenticated_or_redirect_to_login
    unless logged_in?
      flash.alert = 'You must login to perform that action'
      redirect_to login_path
    end
  end

end
