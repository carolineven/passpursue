class SignupsController < ApplicationController

  before_action :authenticated_or_redirect_to_login, :except => [:new, :create]

  def show
    @signup = Signup.find(params[:id])
  end

  def new
    @signup = Signup.new
  end

  def index

  end

  def create
    @signup = Signup.new(user_params)
    if @signup.save
      log_in @signup
      flash[:notice] = "Welcome to the Online Submission!"
      redirect_to @signup
    else
      render 'new'
      flash.now[:danger] = 'New User is not created'
    end
  end

  private
  def user_params
    params.require(:signup).permit(:name, :email, :password,
                                   :password_confirmation)
  end
end

