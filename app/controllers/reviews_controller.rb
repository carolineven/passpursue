class ReviewsController < ApplicationController

  before_action :authenticated_or_redirect_to_login

  def index
    @submission = Submission.find(params[:submission_id])
    @reviews = @submission.reviews
  end

  def new

  end

  def edit
    @review = Review.find(params[:id])
  end

  def update
    @review = Review.find(params[:id])

    if @review.update(review_params)
      redirect_to submission_review_path(@review.submission, @review)
    else
      render :action => 'edit'
    end
  end

  def show
    @review = Review.find(params[:id])
  end

  # TODO ideally this method only works for logged in user.
  # Reviewer ID is current logged in user ; TODO check user permissions to review
  def create
    if current_user.nil?
      redirect_to login_path
    else
      @submission = Submission.find(params[:submission_id])
      @review = @submission.reviews.create(review_params) do |r|
        r.user_id = current_user.id
      end
      if @review.save
        redirect_to(submission_review_path(@review.submission, @review))
      else
        render :action => 'new'
      end
    end
  end

  private
  def review_params
    params.require(:review).permit(:feedback, :verdict)
  end
end
