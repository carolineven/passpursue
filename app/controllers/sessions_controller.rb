class SessionsController < ApplicationController

  def new
  end

  def create
    user = Signup.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in user
      redirect_to submissions_path
    else
      flash.now[:danger] = 'Invalid email/password combination.'
      render 'new'
    end
  end

  def destroy
    log_out
    flash[:notice] = 'You have successfully logged out.'
    redirect_to root_url
  end

end
