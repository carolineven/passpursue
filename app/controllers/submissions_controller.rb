require 'net/http'

class SubmissionsController < ApplicationController

  include FileHelper

  before_action :authenticated_or_redirect_to_login

  def index
    @submissions = Submission.all
  end

  def new
    @submission = Submission.new
  end

  def show
    @submission = Submission.find(params[:id])
  end

  # Only download in the submissions/ folder for now.
  # TODO re-implement to download over HTTP
  def download(submission)
    if (submission.filename != nil)
      user_id = submission.user_id ||= 'user_id_0'
      dir = 'submissions/' + user_id + '/'
      create_dir_struc(dir)
      File.new(dir + submission.filename, 'wb') { |file|
        file.write(submission.file_data)
      }
    end
  end

  # TODO If submission user id is already given and submission user id != current user id:
  #    This is the scenario when an admin adds a submission for a candidate.
  #    Need to check current user permissions to perform that action!
  def create
    @submission = Submission.new(submission_params) do |t|
      t.user_id = params[:user_id] || current_user.id
      if (params[:submission][:file])
        t.filename = ActionController::Base.helpers.sanitize(params[:submission][:file].original_filename)
        t.file_content_type = params[:submission][:file].content_type
        t.file_data = params[:submission][:file].read
      end
      if (params[:submission][:link])
        t.link = params[:submission][:link]
      end
    end
    if @submission.save
      flash[:notice] = 'Submission successfully saved \./'
      redirect_to (@submission)
    else
      flash[:alert] = 'This submission could not be saved /.\\'
      render :action => 'new'
    end
  end

  private
  def submission_params
    params.require(:submission).permit(:link)
  end

end
