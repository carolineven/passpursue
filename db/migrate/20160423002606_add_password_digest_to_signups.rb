class AddPasswordDigestToSignups < ActiveRecord::Migration
  def change
    add_column :signups, :password_digest, :string
  end
end
