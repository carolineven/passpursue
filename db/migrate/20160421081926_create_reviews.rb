class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :user_id #TODO user giving feedback should reference user table
      t.string :feedback, null: false
      t.string :verdict, null: false

      t.references :submission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
