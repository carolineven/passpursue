class CreateSubmissions < ActiveRecord::Migration
  def self.up
    create_table :submissions do |t|

      t.column :user_id, :string
      t.column :link, :string
      t.column :file_content_type, :string
      t.column :filename, :string
      t.column :file_data, :binary

      t.timestamps null: false
    end
  end

  def self.down
    drop_table :submissions
  end
end
